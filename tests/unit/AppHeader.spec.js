import AppHeader from '@/components/AppHeader';
import { mount } from '@vue/test-utils';
// describe creates a block of tests
// aka a test suite
// this is helpful when we have more than one test and we want to group related tests
describe('AppHeader', () => {
  // test creates a Jest test
  // second argument is the function that's run for the test
  test(`If user isn't logged in, don't show logout button`, () => {
    // expect creates an assertion about a test outcome
    // toBe() checks that a value is what we expect
    // wrapper mounts the component and gives us functions to test the component (hence the name 'wrapper')
    var wrapper = mount(AppHeader);
    expect(wrapper.find('button').isVisible()).toBe(false);
  });
  // we need to use async/await to allow component time to mount
  test(`If user is logged in, show logout button`, async () => {
    // expect creates an assertion about a test outcome
    // toBe() checks that a value is what we expect
    var wrapper = mount(AppHeader);
    // use setData to set component data
    wrapper.setData({ loggedIn: true });
    // use nextTick to wait for DOM updates
    await wrapper.vm.$nextTick;
    expect(wrapper.find('button').isVisible()).toBe(true);
  });
});
