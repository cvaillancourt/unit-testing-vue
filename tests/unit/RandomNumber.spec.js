import RandomNumber from '@/components/RandomNumber.vue';
import { mount } from '@vue/test-utils';

describe('RandomNumber', () => {
  test('By default, RandomNumber data value should be 0', () => {
    var wrapper = mount(RandomNumber);
    expect(wrapper.html()).toContain('<span>0</span>');
  });

  test('If button is clicked, RandomNumber data value should be between 1 and 10', async () => {
    var wrapper = mount(RandomNumber);
    // simulate click event
    wrapper.find('button').trigger('click');
    // wait for DOM updates from click event
    await wrapper.vm.$nextTick();
    // get the value in the span
    var randomNumber = parseInt(wrapper.find('span').element.textContent);
    expect(randomNumber).toBeGreaterThanOrEqual(1);
    expect(randomNumber).toBeLessThanOrEqual(10);
  });
  test('If button is clicked, RandomNumber data value should be between 200 and 300', async () => {
    // pass in props data
    const wrapper = mount(RandomNumber, {
      propsData: {
        min: 200,
        max: 300,
      },
    });
    // simulate click event
    wrapper.find('button').trigger('click');
    // wait for DOM updates from click event
    await wrapper.vm.$nextTick();
    // get the value in the span
    const randomNumber = parseInt(wrapper.find('span').element.textContent);
    expect(randomNumber).toBeGreaterThanOrEqual(200);
  });
});
